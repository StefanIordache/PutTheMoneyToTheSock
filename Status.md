# Features

## DISCLAIMER

This is **NOT** the same project as the one we worked through during labs (the money settler one)

You must create a new project with an idea of your own

## Basic requirements:

- [x] At least 5 ActiveRecord models
- [x] At least 2 `has_many` relationships
- [x] At least 1 `has_and_belongs_to_many` relationship
- [x] At least 2 CRUD controllers with views (the whole MVC)
- [x] At least 4 distinct model validations
- [x] Use at least 2 ActiveRecord [scopes](https://api.rubyonrails.org/classes/ActiveRecord/Scoping/Named/ClassMethods.html#method-i-scope)
- [x] Use at least 1 ActiveRecord [enum](https://api.rubyonrails.org/v5.2.1/classes/ActiveRecord/Enum.html)
- [x] Use Devise for users
- [x] Talk about another useful gem for your project

## Functional features:
- [ ] Add a polymorphic relationship between two of your models - 0.5 points  

- [x] Add CRUD operations for a nested resource - 0.5 points

- [x] File upload - 0.5 points  
  Implement file upload for one of your models using either ActiveStorage or a file upload gem for Rails.

- [x] Sending emails - 0.5 points  
  Use ActionMailer to send email notifications to your users.  
  To avoid sending real emails, and inspect sent mails use the [Letter Opener](https://github.com/fgrehm/letter_opener_web) gem.

- [ ] ActiveRecord pagination - 0.5 points  
  Implement pagination using a pagination gem for Rails.

- [ ] Use [respond_to](https://guides.rubyonrails.org/action_controller_overview.html#rendering-xml-and-json-data) - 0.5 points  
  Use `respond_to` with at least two data formats from one of your controllers.

- [ ] RSpec tests with at least 60% coverage - 1 point

- [x] Deploying your application to [Heroku](https://www.heroku.com/) - 1 point

- [x] [Internationalization](https://guides.rubyonrails.org/i18n.html) - 0.5  
  Use i18n features to write different flash messages and menu texts, based on a user set locale.

- [ ] Receiving emails from your Rails app - 1 point

- [ ] Database - adding partial indices - 0.5 point

- [x] Background workers with ActiveJob - 0.5 to 1 point

- [x] Social sign on - 1 point  
  Use a gem for implementing OAuth social signin authentication.

- [x] Bootstrap || Materialize css - 0.5 to 1p  
  Improve the design of your application using a modern CSS framework.

- [ ] Menu navigation gem - 0.5 points

- [x] Charts - 0.5 points  
  Use a JS chart library or Ruby gem wrapper that allows users to visualize data more easily. 

- [x] Creativity & presentation - 1 point

## Submission deadline:

__Must__ be pushed to GitLab before 13 Jan, at midnight.

## Presentation

16 Jan

## Your final grade will be composed of:
* Basic requirements - 4 points
* Functional features - maximum 5 points from any features you find interesting.
* Presence - 1 point
