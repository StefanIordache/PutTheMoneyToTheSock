# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

unless User.exists?

    User.create!(email: 'admin@test.com', password: 'Pa55word', password_confirmation: 'Pa55word')
    
end

unless Expense.exists?
    Expense.create!(title: 'Factura ENEL',
                    description: 'Factura ENEL Ianuarie', 
                    ammount: 35, 
                    expense_date: '2019-01-02', 
                    user_id: 1);
    Expense.create!(title: 'Cumparaturi Auchan',
                    description: 'Cumparaturi Auchan inceput de an', 
                    ammount: 234.5,
                    expense_date: '2019-01-05',
                    user_id: 1);
end