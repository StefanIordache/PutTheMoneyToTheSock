class AddUserToTarget < ActiveRecord::Migration[5.2]
  def change
    add_reference :targets, :user, index: true
  end
end
