class CreateTargets < ActiveRecord::Migration[5.2]
  def change
    create_table :targets do |t|
      t.string :title
      t.string :description
      t.decimal :ammount

      t.timestamps
    end
  end
end
