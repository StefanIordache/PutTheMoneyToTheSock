class AddCurrencyToTargets < ActiveRecord::Migration[5.2]
  def change
    add_column :targets, :currency, :integer, default: 0
  end
end
