class AddTargetToReport < ActiveRecord::Migration[5.2]
  def change
    add_reference :reports, :target, index: true
  end
end
