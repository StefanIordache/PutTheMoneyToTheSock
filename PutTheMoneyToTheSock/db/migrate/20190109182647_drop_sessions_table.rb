class DropSessionsTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :sessions
  end

  def down
    fail ActiveRecord::IrreversibleMigration
  end
end
