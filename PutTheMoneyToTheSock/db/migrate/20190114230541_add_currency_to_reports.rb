class AddCurrencyToReports < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :currency, :integer, default: 0
  end
end
