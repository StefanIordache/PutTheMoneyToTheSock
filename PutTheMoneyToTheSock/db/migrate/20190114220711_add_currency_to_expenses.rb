class AddCurrencyToExpenses < ActiveRecord::Migration[5.2]
  def change
    add_column :expenses, :currency, :integer, default: 0
  end
end
