class Report < ApplicationRecord
    enum currency: [ :ron, :euro, :usd ]

    validates :title, presence: true, length: { minimum: 1, maximum: 500}
    validates :from_date, presence: true
    validates :to_date, presence: true
    validates :from_date, date: { before_or_equal_to: :to_date}
    validates :description, length: { maximum: 2000}
    validate :report_and_target_currency_match

    belongs_to :user
    belongs_to :target
    has_and_belongs_to_many :expenses

    def report_and_target_currency_match
        if currency != target.currency
          errors.add("Can't match currencies between report and target")
        end
      end

end
