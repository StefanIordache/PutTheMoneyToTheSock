class Target < ApplicationRecord

    enum currency: [ :ron, :euro, :usd ]

    validates :title, presence: true, length: { minimum: 1, maximum: 500}
    validates :ammount, presence: true
    validates :description, length: { maximum: 2000}

    belongs_to :user
    has_many :reports
end
