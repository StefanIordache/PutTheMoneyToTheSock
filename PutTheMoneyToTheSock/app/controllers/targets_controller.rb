class TargetsController < ApplicationController
  before_action :authenticate_user!

  def index

    if current_user
      @targets = Target.where(user_id: current_user.id)
    end
  end
    
  def show

    @target = Target.find(params[:id])

  end

  def edit

    @button_text = "Update"
    @target = Target.find(params[:id])

  end

  def update

    @target = Target.find(params[:id])

    if @target.update_attributes(target_params)
      redirect_to action: 'index'
    else
      render 'edit'
    end

  end

  def new

    @button_text = "Create"
    @target = Target.new

  end

  def create

    @target = Target.new(target_params)

    if current_user
      @target.user_id = current_user.id
    else
      render 'new'
    end

    if @target.save
      redirect_to action: 'index'
    else
      render 'new'
    end

  end

  def destroy

    @target = Target.find(params[:id])
    @target.destroy

    flash[:success] = "Target deleted!"
  
    redirect_to targets_path

  end

  private

    def target_params
      params.require(:target).permit(:title, :description, :ammount, :currency)
    end

end
