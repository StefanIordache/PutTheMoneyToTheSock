class ApplicationController < ActionController::Base
    #before_action :authenticate_user!
    protect_from_forgery

    prepend_before_action :set_locale

    before_action :set_locale

    def default_url_options
        { locale: I18n.locale }
    end

    def set_locale
        I18n.locale = params[:locale] || I18n.default_locale
    end
end
